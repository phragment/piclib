
import collections
import threading

import piclib.view
import piclib.model


class Operator:

    def __init__(self, log, settings):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings

        self.queue = Queue()
        self.running = True

        self.view = piclib.view.View(log, settings, self)
        self.model = piclib.model.Model(log, settings, self.view)

    def start(self):
        op_thread = threading.Thread(target=self.process, name="OpThread")
        op_thread.start()

        # Gtk has to be run from thread which created the objects
        self.view.start()

        op_thread.join()
        self.log.debug("stopped")

    def process(self):
        self.log.debug("starting in " + threading.current_thread().name)
        while self.running:
            event = self.queue.get()

            if not isinstance(event, piclib.events.Event):
                self.log.warning("unknown event: " + str(event))
                continue

            event.process(self)

    def stop(self):
        self.log.debug("stopping")
        self.running = False

    def enqueue(self, cmd):
        #self.log.debug("exec " + str(cmd))
        self.queue.put(cmd)


# command queue
class Queue:

    def __init__(self):
        #self.queue = []
        # thread-safe, memory efficient and fast
        # appends and pops from either side
        self.queue = collections.deque()
        # not for locking, consumer sleeps on this
        self.sem = threading.Semaphore(value=0)

    def put(self, cmd):
        # TODO allow commands to inhibit cmd processing
        # TODO allow dumping of new commands?
        self.queue.append(cmd)
        #print(self.queue)
        self.sem.release()

    def get(self):
        self.sem.acquire()
        cmd = self.queue[0]
        del self.queue[0]
        return cmd

