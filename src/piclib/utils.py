import subprocess


def run(cmd, stdin=None, cwd=None, timeout=None):

    proc = subprocess.Popen(cmd, cwd=cwd)


def run_(cmd, stdin=None, cwd=None, timeout=None):
    # blocking!
    proc = subprocess.Popen(cmd, cwd=cwd, stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        out, err = proc.communicate(input=stdin, timeout=timeout)
    except subprocess.TimeoutExpired:
        proc.kill()
        out, err = proc.communicate()
    ret = out.decode(errors="replace").strip()
    if not ret:
        ret = err.decode().strip()
    return proc.returncode, ret

