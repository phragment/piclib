
import os
import threading

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gdk, GLib

import piclib.imgview
import piclib.sidepane
import piclib.events

import piclib.model

## View
# this encapsulates Gtk loops / thread
#
# all public methods in view should be thread safe
# regarding external calls
#

# decorator for enqueuing function into gtk thread
def gtk(func):
    def wrapper(*args, **kwargs):
        #print("view wrapper, called by", threading.current_thread())
        GLib.idle_add(func, *args, **kwargs)
    return wrapper


class View:

    def __init__(self, log, settings, ctrl):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings
        self.ctrl = ctrl

        self.app = Application(self)

    def start(self):
        self.log.debug("starting in %s", threading.current_thread().name)
        self.app.run()
        self.log.debug("stopped")

    @gtk
    def stop(self):
        self.log.debug("stopping")
        #self.app.mainwin.window.close()  # just raises close-request?
        #self.log.debug("window.close() returned")
        self.app.mainwin.window.destroy()

    @gtk
    def view_img(self, fp, img):
        self.app.mainwin.view_img(fp, img)

    @gtk
    def hide_img(self):
        self.app.mainwin.hide_img()

    @gtk
    def add_img(self, cimg):
        self.app.mainwin.imgview.add(cimg)

    @gtk
    def remove_img(self, fp):
        self.app.mainwin.imgview.remove(fp)

    @gtk
    def clear_flowbox(self):
        self.app.mainwin.imgview.clear()

    @gtk
    def add_dir(self, dn):
        self.app.mainwin.folders.add(dn)

    @gtk
    def remove_dir(self, dp):
        self.app.mainwin.folders.remove(dp)

    @gtk
    def update_tags(self, tags):
        self.app.mainwin.update_tags(tags)

    @gtk
    def clear_dir(self):
        self.app.mainwin.folders.clear()

    def get_selected(self):
        return self.app.mainwin.get_selected()

    @gtk
    def filter_tags(self, tags):
        self.app.mainwin.imgview.set_visible(tags)

    @gtk
    def update_title(self, dp):
        self.app.mainwin.set_title(dp)

    def on_selection_changed(self, sel):
        if not sel:
            return
        cimg = sel[0]
        # TODO unify call
        self.app.mainwin.sidepane.page_edit.set_tags(cimg.tags)
        self.app.mainwin.sidepane.page_edit.set_desc(cimg.desc)

        self.app.mainwin.sidepane.page_search.infobox.set_info(cimg)


class Application:

    def __init__(self, view):
        self.view = view
        self.app = Gtk.Application.new(None, 0)

        appid = "de.elektronenpumpe.piclib.pid{}".format(os.getpid())
        self.app.set_property("application-id", appid)

        self.app.connect("activate", self.activate)

    def run(self):
        self.app.run()

    def activate(self, app):
        self.mainwin = MainWindow(app, self.view)


class MainWindow:

    def __init__(self, app, view):
        view.mainwindow = self

        self.view = view
        self.ctrl = view.ctrl
        self.log = view.log

        self.window = Gtk.ApplicationWindow.new(app)
        self.window.set_title("PicLib")
        self.window.set_icon_name("image-x-generic")
        self.window.connect("close-request", self.on_window_close_request)

        if self.view.settings["adaptive"]:
            self.setup_headerbar()
            #self.window.maximize()
            self.window.set_default_size(900, 1020)
        else:
            f = self.window.get_scale_factor()
            #self.window.set_size_request(800, 600)
            self.window.set_default_size(1280 * f, 720 * f)

        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.window.set_child(self.vbox)

        self.box = Gtk.Box()
        self.box.set_hexpand(True)
        self.box.set_property("halign", Gtk.Align.FILL)
        self.vbox.append(self.box)

        # TODO rethink, move to respective widgets?
        #self.toolbar = Toolbar(self.view)
        #self.vbox.append(self.toolbar.get_widget())

        self.folders = Folders(self.view)
        self.box.append(self.folders.get_widget())

        self.viewer = Viewer(self.view)
        self.box.append(self.viewer.get_widget())

        self.imgview = piclib.imgview.ImgView(self.view)
        self.box.append(self.imgview.get_widget())

        self.sidepane = piclib.sidepane.SidePane(self.view)
        self.box.append(self.sidepane.get_widget())

        self.window.show()
        self.viewer.hide()

        if self.view.settings["adaptive"]:
            self.sidepane.hide()
            self.folders.hide()

        self.log.debug("startup done?")
        # TODO set focus
        self.imgview.flowbox.grab_focus()
        # TODO select first item?

    def setup_headerbar(self):
        self.headerbar = Gtk.HeaderBar.new()
        #self.headerbar.set_title("PicLib")
        self.headerbar.set_show_title_buttons(False)

        self.titlebox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.title = Gtk.Label.new("PicLib")
        self.title.set_vexpand(True)
        self.title.add_css_class("title")
        self.titlebox.append(self.title)
        self.subtitle = Gtk.Label.new("")
        self.subtitle.set_vexpand(True)
        self.subtitle.add_css_class("subtitle")
        self.titlebox.append(self.subtitle)
        self.headerbar.set_title_widget(self.titlebox)

        self.window.set_titlebar(self.headerbar)

        button_back = Gtk.Button.new_from_icon_name("go-previous")
        button_back.set_tooltip_text("Go Back")
        button_back.connect("clicked", self.on_hbb_back_clicked)
        self.headerbar.pack_start(button_back)

        self.button_overflow = Gtk.Button.new_from_icon_name("view-more")
        self.button_overflow.set_tooltip_text("Overflow")
        self.button_overflow.connect("clicked", self.on_hbb_overflow_clicked)
        self.headerbar.pack_end(self.button_overflow)

        self.button_tabs = Gtk.Button.new_from_icon_name("view-list-bullet")
        self.button_tabs.set_tooltip_text("Show Dirs")
        self.button_tabs.connect("clicked", self.on_hbb_folders_clicked)
        self.headerbar.pack_end(self.button_tabs)

        self.popover = Gtk.PopoverMenu()
        self.popover.set_parent(self.button_overflow)
        self.popover.set_position(Gtk.PositionType.BOTTOM)

        self.pobox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.popover.set_child(self.pobox)

        button_tags = self.create_button("Tagging", "bookmark-new", self.on_hbb_tags_clicked)
        self.pobox.append(button_tags)

        button_exit = self.create_button("Exit", "window-close", self.on_window_close_request)
        self.pobox.append(button_exit)

    def create_button(self, label_text, icon_name, cb):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        icon = Gtk.Image.new_from_icon_name(icon_name)
        box.append(icon)

        label = Gtk.Label.new(label_text)
        label.props.xalign = 0  # left
        label.props.hexpand = True
        label.props.margin_start = 5
        box.append(label)

        button = Gtk.Button.new()
        button.add_css_class("flat")
        button.set_child(box)

        button.connect("clicked", cb)

        return button

    def on_hbb_overflow_clicked(self, button):
        self.popover.popup()

    def on_hbb_folders_clicked(self, button):
        self.folders.toggle()

    def on_hbb_tags_clicked(self, button):
        # TODO
        pass

    def on_hbb_back_clicked(self, button):
        # TODO make context sensitive, close img when viewer is active, go back/up in folders otherwise?
        self.view.ctrl.queue.put(piclib.events.HideImg())

    def set_title(self, dp):
        title = "PicLib"
        if dp:
            dn = os.path.basename(dp)
            title += " - " + dn
        self.window.set_title(title)

        if self.view.settings["adaptive"]:
            self.title.set_text("PicLib")
            self.subtitle.set_text(dp)

    def on_window_close_request(self, window):
        self.log.debug("window close request")
        self.ctrl.queue.put(piclib.events.Quit())
        return True

    def update_tags(self, tags):
        self.sidepane.page_search.tags_ls.clear()
        self.sidepane.page_edit.tags_ls.clear()
        for tag in tags:
            self.sidepane.page_search.tags_ls.append([tag])
            self.sidepane.page_edit.tags_ls.append([tag])

    def get_selected(self):
        # returns filenames
        return self.imgview.get_selected()

    # TODO move to View() ?
    def view_img(self, fp, img):
        self.viewer.view_img(fp, img)
        self.viewer.show()
        self.imgview.hide()

    def hide_img(self):
        self.viewer.hide()
        self.imgview.show()


# TODO better in gtk4?
# https://docs.gtk.org/gtk4/class.Picture.html
# https://blog.gtk.org/2020/05/20/media-in-gtk-4/

# https://stackoverflow.com/questions/18841430/implementing-an-image-viewer-that-can-do-scrolling-as-well-as-responsive-scaling

class Viewer:

    def __init__(self, view):
        self.view = view
        self.log = view.log.getChild(type(self).__name__)

        self.box = Gtk.Box()

        self.sw = Gtk.ScrolledWindow()
        self.box.append(self.sw)

        self.bbox = Gtk.Box()
        self.sw.set_child(self.bbox)

        self.fp_cur = ""

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-pressed", self.on_key)
        #self.box.add_controller(ec_key)
        self.view.mainwindow.window.add_controller(ec_key)

        ec_swipe = Gtk.GestureSwipe()
        ec_swipe.connect("swipe", self.on_swipe)
        self.view.mainwindow.window.add_controller(ec_swipe)

        ec_click = Gtk.GestureClick()
        ec_click.connect("released", self.on_click)
        self.view.mainwindow.window.add_controller(ec_click)

    def hide(self):
        self.box.set_visible(False)

    def show(self):
        self.box.set_visible(True)

    def get_widget(self):
        return self.box

    def view_img(self, fp, pb):
        self.log.debug("show img %s %s", fp, pb)

        #img = Gtk.Image.new_from_pixbuf(pb)
        img = Gtk.Picture.new_for_pixbuf(pb)  # gtk4

        img.set_property("hexpand", True)
        img.set_property("vexpand", True)
        img.set_property("halign", Gtk.Align.FILL)
        img.set_property("valign", Gtk.Align.FILL)

        # clear box
        child = self.bbox.get_first_child()
        while child:
            self.bbox.remove(child)
            child = child.get_next_sibling()

        self.bbox.append(img)
        self.fp_cur = fp

    def on_key(self, ec, keyval, keycode, state):
        if not self.box.is_visible():
            return False

        if keyval == Gdk.KEY_Escape:
            self.view.ctrl.queue.put(piclib.events.HideImg())
            return True

        if keyval == Gdk.KEY_Left:
            self.view.ctrl.queue.put(piclib.events.PrevImg(self.fp_cur))
            return True

        if keyval == Gdk.KEY_Right:
            self.view.ctrl.queue.put(piclib.events.NextImg(self.fp_cur))
            return True

    def on_swipe(self, gesture, vx, vy):
        #self.log.debug("swipe %s %s", vx, vy)

        if not self.box.is_visible():
            return False

        # threshold
        if not abs(vx) > 10:
            return False

        if vx > 0:
            self.view.ctrl.queue.put(piclib.events.PrevImg(self.fp_cur))
            return True

        if vx < 0:
            self.view.ctrl.queue.put(piclib.events.NextImg(self.fp_cur))
            return True

    def on_click(self, gesture, n_press, x, y):
        if not self.box.is_visible():
            return False

        if n_press == 2:
            self.view.ctrl.queue.put(piclib.events.HideImg())
            return True


class Folders:

    # TODO hide if no folders?
    # hide by default in adaptive

    def __init__(self, view):
        self.view = view
        self.log = view.log.getChild(type(self).__name__)

        self.sw = Gtk.ScrolledWindow()
        self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        self.listbox = Gtk.ListBox()
        self.sw.set_child(self.listbox)

        self.children = {}

        self.listbox.connect("row-activated", self.on_row_activated)

        ## dnd
        actions = Gdk.DragAction.ASK
        actions |= Gdk.DragAction.COPY
        actions |= Gdk.DragAction.MOVE

        # drop
        # TODO has ask to be implemented in handler?
        self.ctrl_drop = Gtk.DropTarget()
        self.ctrl_drop.set_actions(actions)
        self.ctrl_drop.set_gtypes( [Gdk.FileList, str] )
        self.ctrl_drop.connect("drop", self.on_drop)
        self.listbox.add_controller(self.ctrl_drop)

    def get_widget(self):
        return self.sw

    def hide(self):
        self.sw.set_visible(False)

    def show(self):
        self.sw.set_visible(True)

    def toggle(self):
        if self.sw.get_visible():
            self.sw.set_visible(False)
        else:
            self.sw.set_visible(True)

    def add(self, dp):
        if self.children.get(dp, None):
            self.log.debug("folder already listed %s", dp)
            return

        self.log.debug("add folder %s", dp)

        box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 10)
        img = Gtk.Image.new_from_icon_name("folder")
        box.append(img)
        dn = os.path.split(dp)[-1]
        label = Gtk.Label.new(dn)
        box.append(label)

        img.set_property("hexpand", True)
        img.set_property("vexpand", True)
        img.set_property("halign", Gtk.Align.FILL)
        img.set_property("valign", Gtk.Align.FILL)
        img.set_property("width-request", 50)

        row = Gtk.ListBoxRow()
        row._info = {"path": dp}
        row.set_child(box)
        self.children[dp] = row
        self.listbox.append(row)

    def clear(self):
        self.log.debug("remove all folders")
        for dp in list(self.children):
            self.remove(dp)

    def remove(self, dp):
        self.log.debug("remove folder %s", dp)
        row = self.children.get(dp, None)
        if not row:
            return
        self.listbox.remove(row)
        del self.children[dp]

    def on_row_activated(self, box, row):
        for dp, r in self.children.items():
            if r == row:
                self.view.ctrl.queue.put(piclib.events.SwitchDir(dp))

    def on_drop(self, ctrl, value, x, y):
        self.log.debug("drop")

        files = []
        if isinstance(value, Gdk.FileList):
            gfiles = value.get_files()

            for gfile in gfiles:
                files.append(gfile.get_path())

        if isinstance(value, str):
            files.append(value)

        row = self.listbox.get_row_at_y(y)
        dp = row._info["path"]

        self.view.ctrl.queue.put(piclib.events.Move(files, dp))


class Toolbar:

    def __init__(self, view):
        self.view = view

        self.box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 1)
        self.box.set_name("toolbar")

        #self.back = Gtk.Button.new_from_icon_name("go-previous", Gtk.IconSize.MENU)
        self.back = Gtk.Button.new_from_icon_name("go-previous")
        self.back.connect("clicked", self.on_go_back)
        #self.box.pack_start(self.back, False, False, 0)
        self.box.append(self.back)

        #self.open_file = Gtk.Button.new_from_icon_name("document-open", Gtk.IconSize.MENU)
        self.open_file = Gtk.Button.new_from_icon_name("document-open")
        self.open_file.connect("clicked", self.on_open_file)
        #self.box.pack_start(self.open_file, False, False, 0)
        self.box.append(self.open_file)

        #self.fit = Gtk.Button.new_from_icon_name("zoom-fit-best", Gtk.IconSize.MENU)
        self.fit = Gtk.Button.new_from_icon_name("zoom-fit-best")
        #self.fit.connect("clicked", self.on_)
        #self.box.pack_start(self.fit, False, False, 0)
        self.box.append(self.fit)

        #self.orig = Gtk.Button.new_from_icon_name("zoom-original", Gtk.IconSize.MENU)
        self.orig = Gtk.Button.new_from_icon_name("zoom-original")
        #self.back.connect("clicked", self.on_)
        #self.box.pack_start(self.orig, False, False, 0)
        self.box.append(self.orig)

        self.close = Gtk.Button.new_from_icon_name("window-close")
        self.close.connect("clicked", self.on_close_viewer)
        self.box.append(self.close)

    def get_widget(self):
        return self.box

    def on_go_back(self, button):
        self.view.ctrl.queue.put(piclib.events.SwitchDir(".."))

    def on_close_viewer(self, button):
        self.view.ctrl.queue.put(piclib.events.HideImg())

    def on_open_file(self, button):
        #fp = self.view.mainwindow.viewer.cur_fp
        #if not fp:
        #    return

        fps = self.view.app.mainwin.get_selected()
        if not fps:
            return
        fp = fps[0]

        self.view.ctrl.queue.put(piclib.events.OpenFile(fp))















