import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gdk, Gio

import piclib.events


class ImgView:

    def __init__(self, view):
        self.view = view
        self.ctrl = view.ctrl
        self.log = view.log

        self.box = Gtk.Box()

        self.scrolledwindow = Gtk.ScrolledWindow()
        self.box.append(self.scrolledwindow)

        self.flowbox = Gtk.FlowBox()
        self.scrolledwindow.set_child(self.flowbox)

        self.flowbox.props.activate_on_single_click = False
        self.flowbox.set_selection_mode(Gtk.SelectionMode.MULTIPLE)

        self.flowbox.connect("child-activated", self.on_flowbox_child_activated)

        # TODO make smarter or at least configurable
        #self.flowbox.set_max_children_per_line(4)

        # FIXME can i set a minimum child size?

        self.flowbox.set_property("hexpand", True)
        self.flowbox.set_property("vexpand", True)
        self.flowbox.set_property("halign", Gtk.Align.FILL)
        self.flowbox.set_property("valign", Gtk.Align.FILL)

        self.flowbox.set_sort_func(self.sort)
        self.flowbox.set_filter_func(self.filter)

        self.flowbox.connect("selected-children-changed", self.on_selection_changed)

        self.tags = []
        self.children = {}
        self.sort_mode = "mtime"

        ## dnd
        actions = Gdk.DragAction.ASK
        actions |= Gdk.DragAction.COPY
        actions |= Gdk.DragAction.MOVE

        # drag
        self.ctrl_drag = Gtk.DragSource()
        self.ctrl_drag.set_actions(actions)
        self.ctrl_drag.connect("prepare", self.on_drag_prepare)
        self.flowbox.add_controller(self.ctrl_drag)

    def get_widget(self):
        return self.box

    def show(self):
        self.box.show()

    def hide(self):
        self.box.hide()

    def sort(self, child1, child2):
        if self.sort_mode == "mtime":
            return self.sort_mtime(child1, child2)

        if self.sort_mode == "alpha":
            return self.sort_alpha(child1, child2)


        return 0

    def sort_mtime(self, child1, child2):
        mtime1 = child1.get_child()._cimg.mtime
        mtime2 = child2.get_child()._cimg.mtime

        if mtime1 < mtime2:
            return 1   # if child1 should be behind child2

        if mtime1 > mtime2:
            return -1  # if child1 should be before child2

        return 0   # if equal

    def sort_alpha(self, child1, child2):
        fp1 = child1.get_child()._cimg.fp_content
        fp2 = child2.get_child()._cimg.fp_content

        if fp1 > fp2:
            return 1   # if child1 should be behind child2

        if fp1 < fp2:
            return -1  # if child1 should be before child2

        return 0   # if equal

    def set_visible(self, tags):
        self.log.debug("show images tagged by %s", tags)
        self.tags = tags
        self.flowbox.invalidate_filter()

    def filter(self, child):
        # show all images if no tags set
        if not self.tags:
            return True

        wdgt = child.get_child()
        tags = wdgt._cimg.tags

        #print(tags, self.tags)
        if all(tag in tags for tag in self.tags):
            return True

        return False  # invisible

    def add(self, cimg):
        fp = cimg.fp

        #self.log.debug("add image %s", fp)
        image = Gtk.Image.new_from_pixbuf(cimg.thumb)
        thumbsize = self.view.ctrl.model.cache.thumbsize
        image.set_property("height-request", thumbsize)
        image.set_property("width-request", thumbsize)

        image._cimg = cimg

        fbchild = Gtk.FlowBoxChild()
        fbchild.set_child(image)
        self.children[fp] = fbchild
        self.flowbox.insert(fbchild, -1)

    def remove(self, fp):
        self.log.debug("remove image %s", fp)
        fbchild = self.children.get(fp, None)
        if not fbchild:
            return
        self.flowbox.remove(fbchild)
        del self.children[fp]

    def clear(self):
        self.log.debug("remove all images")
        self.flowbox.select_all()
        for child in self.flowbox.get_selected_children():
            self.flowbox.remove(child)

    def get_selected(self):
        sel_fb_children = self.flowbox.get_selected_children()
        sel_fn = []
        for child in sel_fb_children:
            sel_fn.append(child.get_child()._cimg.fp)
        return sel_fn

    def get_selected_info(self):
        sel_fb_children = self.flowbox.get_selected_children()
        sel_fn = []
        for child in sel_fb_children:
            sel_fn.append(child.get_child()._cimg)
        return sel_fn

    def on_flowbox_child_activated(self, flowbox, fbchild):
        wdgt = fbchild.get_child()
        fp = wdgt._cimg.fp
        fp_content = wdgt._cimg.fp_content
        if fp == fp_content:
            self.ctrl.queue.put(piclib.events.ShowImg(fp))
        else:
            cur_dir = self.view.ctrl.model.cur_dir
            if cur_dir.endswith(".zip") or cur_dir.endswith(".cbz"):
                self.ctrl.queue.put(piclib.events.ShowImg(fp + "?" + fp_content))
            else:
                self.ctrl.queue.put(piclib.events.SwitchDir(fp))

    def on_drag_prepare(self, ctrl, x, y, test=""):
        self.log.debug("drag prepare: %s", test)

        paths = self.get_selected()
        gfiles = []
        for fp in paths:
            gfile = Gio.File.new_for_path(fp)
            gfiles.append(gfile)

        if not gfiles:
            self.log.debug("nothing selected")
            return None

        file_list = Gdk.FileList.new_from_list(gfiles)
        content_provider = Gdk.ContentProvider.new_for_value(file_list)
        return content_provider

    def on_selection_changed(self, flowbox):
        sel = self.get_selected_info()
        self.view.on_selection_changed(sel)







