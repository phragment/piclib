import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

import piclib.events


# TODO replace treeview
class EditPage:

    def __init__(self, ctrl):
        self.ctrl = ctrl

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.tags_ls = Gtk.ListStore(str)
        self.tags_tv = Gtk.TreeView(model=self.tags_ls)
        self.tags_tv.set_property("halign", Gtk.Align.FILL)
        self.box.append(self.tags_tv)

        cr = Gtk.CellRendererText()
        # text=0  set cellrenderer attribute "text" to model column 0
        col = Gtk.TreeViewColumn("Tags", cr, text=0)
        col.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        self.tags_tv.append_column(col)

        self.tags_entry = Gtk.Entry()
        self.tags_entry.set_property("halign", Gtk.Align.FILL)
        #self.box.append(self.tags_entry)
        self.tags_frame = Gtk.Frame()
        self.tags_frame.set_label("Tags")
        self.tags_frame.set_child(self.tags_entry)
        self.box.append(self.tags_frame)

        self.desc_entry = Gtk.Entry()
        self.desc_entry.set_property("halign", Gtk.Align.FILL)
        #self.box.append(self.desc_entry)
        self.desc_frame = Gtk.Frame()
        self.desc_frame.set_label("Description")
        self.desc_frame.set_child(self.desc_entry)
        self.box.append(self.desc_frame)

        self.tags_entry.connect("activate", self.on_tags_entry_activated)
        self.tags_tv.connect("row-activated", self.on_tags_tv_activated)
        self.desc_entry.connect("activate", self.on_desc_entry_activated)

    def get_widget(self):
        return self.box

    def set_tags(self, tags):
        cur = ",".join(tags)
        self.tags_entry.set_text(cur)

    def set_desc(self, desc):
        self.desc_entry.set_text(desc)

    def on_tags_entry_activated(self, entry):
        # parsing in controller
        tags_raw = entry.get_text()
        tags = [tag.strip() for tag in tags_raw.split(",")]
        # TODO better way to access method of object "above" ?!
        # try using parent?
        imgs = self.ctrl.view.get_selected()
        #print(imgs, tags)  # TODO use log
        self.ctrl.queue.put(piclib.events.ImgSetTags(imgs, tags))

    def on_tags_tv_activated(self, tv, path, col):
        selection = tv.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        it = model.get_iter(pathlist[0])
        tag = model.get_value(it, 0)

        imgs = self.ctrl.view.get_selected()
        #print("tag", imgs, "with", tag)  # TODO use log
        self.ctrl.queue.put(piclib.events.ImgAddTags(imgs, [tag]))

    def on_desc_entry_activated(self, entry):
        desc = entry.get_text()
        desc = desc.strip()  # TODO lr strip
        imgs = self.ctrl.view.get_selected()
        self.ctrl.queue.put(piclib.events.ImgSetDesc(imgs, desc))





