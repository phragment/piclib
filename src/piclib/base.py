# This file is part of PicLib
# Copyright 2019-2023 Thomas Krug
#
# PicLib is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PicLib is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

"""intended as common base for my python applications
"""

import argparse
import configparser
import logging
import os
import signal
import sys
import threading
import traceback


def mkdir(dirpath):
    """create directory
    """
    try:
        os.mkdir(dirpath)
    except FileExistsError:
        pass


class Application:
    """base for a command line and or gui application

    start is called with log and settings
    settings are a dict containing only sanitized values, specified in the scheme

    - str
    - (conf)path
    - datapath
    - int
    - float
    - bool
    """
    # pylint: disable=too-many-instance-attributes

    def __init__(self, name, scheme, start, stop):
        self.scheme = {
            "verbose": {"type": "bool", "default": False},
            "quiet": {"type": "bool", "default": True},
            "filter": {"type": "str", "default": "",
                       "desc": "only show log messages from specified unit"},
            "config": {"type": "path", "default": "",
                       "desc": "read specific config file"}
        }

        self.name = name
        if scheme:
            self.scheme.update(scheme)
        self.start = start
        self.stop = stop

        self.log = self.init_logging()
        self.log.debug("initializing app")

        self.setup_exceptions()
        self.setup_signals()

        self.option_parser = OptionParser(self.log, self.name, self.scheme)

        options = self.option_parser.parse()
        self.update_logging(options)

        self.config_parser = ConfigParser(self.log, self.name, self.scheme,
                                          options["config"])
        config = self.config_parser.parse()

        self.settings = self.merge_settings(options, config)
        self.log.debug("settings: %s", self.settings)
        self.update_logging(self.settings)

    def merge_settings(self, arguments, config):
        """creates settings dict by merging cmdline arguments, config and defaults

        priorities (low to high)
        1. default
        2. config
        3. cmdline

        treats None returned by arguments or config as invalid / not set
        """
        settings = {}

        self.log.debug("merging settings")
        for name in self.scheme:
            self.log.debug("setting %s", name)

            if arguments.get(name, None) is not None:
                settings[name] = arguments[name]
                self.log.debug("  argument: %s", arguments[name])

            elif config.get(name, None) is not None:
                settings[name] = config[name]
                self.log.debug("  config: %s", config[name])

            else:
                p = self.scheme[name]["default"]

                if self.scheme[name]["type"] == "path":
                    # prepend conf dir, if not abs
                    if not os.path.isabs(p):
                        p = os.path.join(self.config_parser.dir, p)

                if self.scheme[name]["type"] == "datapath":
                    if not os.path.isabs(p):
                        data = os.getenv("XDG_DATA_HOME")
                        if not data:
                            data = os.path.expanduser("~/.local/share")
                            data = os.path.join(data, self.name)
                        p = os.path.join(data, p)

                settings[name] = p
                self.log.debug("  default: %s", p)

        #
        settings["config"] = self.config_parser.file

        return settings

    def init_logging(self):
        """initialize logging framework

        on linux we log to stderr

        on windows we log to %APPDATA%/appname/logfile.txt

        """
        log = logging.getLogger(self.name)
        log.propagate = False

        handler = None

        if sys.platform == "win32":
            appdata = os.path.expandvars("%APPDATA%")
            logfile = os.path.join(appdata, self.name, "logfile.txt")
            mkdir(os.path.dirname(logfile))
            handler = logging.FileHandler(logfile, mode="w")  # overwrite dont append

        else:
            interactive = sys.__stdout__.isatty()
            if not interactive:
                # try to use the journal
                try:
                    import systemd.journal
                    handler = systemd.journal.JournalHandler(SYSLOG_IDENTIFIER=self.name)
                except ModuleNotFoundError:
                    pass

            if not handler:
                # stdout
                #handler = logging.StreamHandler()
                # stderr
                handler = logging.StreamHandler(stream=sys.stderr)

        formatter = logging.Formatter("%(name)s: %(message)s")

        handler.setFormatter(formatter)
        log.addHandler(handler)

        log.setLevel(logging.ERROR)

        # for debugging app base
        #log.setLevel(logging.DEBUG)

        return log

    def update_logging(self, settings):
        """update log settings
        """
        if settings["filter"]:
            filter_name = settings["filter"]
            log_filter = logging.Filter(filter_name)
            self.log.addFilter(log_filter)

        if settings["verbose"]:
            self.log.setLevel(logging.DEBUG)
        elif settings["quiet"]:
            self.log.setLevel(logging.ERROR)
        else:
            self.log.setLevel(logging.INFO)

    def setup_exceptions(self):
        """set up exception hooks
        """
        sys.excepthook = self.on_exception
        threading.excepthook = self.on_thread_exception

    def setup_signals(self):
        """install handlers for sigint (and hup)
        """
        signal.signal(signal.SIGINT, self.on_sigint)
        if sys.platform != "win32":
            signal.signal(signal.SIGHUP, signal.SIG_IGN)

    def on_exception(self, etype, value, trace):
        """exception callback
        """
        # use lazy %-formatting
        # "crash in %s" % threading.current_thread().name
        self.log.error("crash in %s", threading.current_thread().name)
        exception_trace = traceback.format_exception(etype, value, trace)
        self.log.error("".join(exception_trace))

        if sys.platform == "win32":
            os.kill(os.getpid(), signal.SIGTERM)
        else:
            os.kill(os.getpid(), signal.SIGKILL)

    def on_thread_exception(self, args):
        """exception callback for threading
        """
        self.on_exception(args.exc_type, args.exc_value, args.exc_traceback)

    def on_sigint(self, signum, frame):  # pylint: disable=unused-argument
        """signal callback
        """
        self.log.debug("got sigint")
        self.stop()

    def run(self):
        """start the main function
        """
        return self.start(self.log, self.settings)


class OptionParser:
    """abstraction around pythons argparse module
    """

    def __init__(self, log, name, scheme):
        self.log = log.getChild(type(self).__name__)
        self.name = name
        self.scheme = scheme

        self.parser = argparse.ArgumentParser(prog=self.name, conflict_handler="resolve")

        self.init_options()

    def init_options(self):
        """add command line options for scheme

        return None as default
        """
        self.log.debug("initializing options")

        for name, opt in self.scheme.items():
            type_ = opt["type"]
            desc = opt.get("desc", "")
            pos = opt.get("positional", "")

            if pos == "opt":
                self.parser.add_argument(name, nargs="?", help=desc, default=None)
                continue

            if pos == "optlist":
                self.parser.add_argument(name, nargs="*", help=desc, default=None)
                continue

            if pos == "req":
                self.parser.add_argument(name, help=desc, default=None)
                continue

            short = "-" + name[0]
            long_ = "--" + name

            if type_ == "str":
                self.parser.add_argument(short, long_, type=str, help=desc, default=None)

            if type_ == "path" or type_ == "datapath":
                self.parser.add_argument(short, long_, type=str, help=desc, default=None)

            if type_ == "int":
                self.parser.add_argument(short, long_, type=int, help=desc, default=None)

            if type_ == "float":
                self.parser.add_argument(short, long_, type=float, help=desc, default=None)

            # TODO test
            if type_ == "bool":
                if opt["default"]:
                    self.parser.add_argument(short, long_, action="store_false", help=desc, default=None)
                else:
                    self.parser.add_argument(short, long_, action="store_true", help=desc, default=None)

    def parse(self):
        """parse command line options according to scheme

        filepath in options is relative to current working dir (at time of parsing)
        """
        self.log.debug("parsing options")
        namespace = self.parser.parse_args()

        # turns auto_git into auto-git
        arguments = {}
        for name, opt in vars(namespace).items():
            arguments[name.replace("_", "-")] = opt

        config_dir = os.getcwd()

        # TODO move path handling to own class?
        for name, opt in self.scheme.items():
            if opt["type"] == "path":
                filepath = arguments.get(name, None)
                if not filepath:
                    continue
                if not os.path.isabs(filepath):
                    filepath = os.path.join(config_dir, filepath)
                arguments[name] = os.path.normpath(filepath)

        self.log.debug("done %s", arguments)
        return arguments


class ConfigParser:
    """abstraction around pythons configparser module
    """

    def __init__(self, log, name, scheme, filepath):
        self.log = log.getChild(type(self).__name__)
        self.name = name
        self.scheme = scheme

        self.parser = configparser.ConfigParser()

        if filepath:
            self.file = filepath
        else:
            dirs = self.init_dirs()
            self.file = self.find_config(dirs)

    def init_dirs(self):
        """initialize config directories according to platform
        """
        self.log.debug("initializing config directories")

        if sys.platform == "win32":
            dirs = self.init_dirs_windows()
        else:
            dirs = self.init_dirs_linux()

        self.log.debug("done {}".format(dirs))
        return dirs

    def init_dirs_linux(self):
        """initialize config directories for linux
        """
        dirs = []

        # user
        user = os.getenv("XDG_CONFIG_HOME")
        if not user:
            # seems to use $HOME from env
            user = os.path.expanduser("~/.config")
        user = os.path.join(user, self.name)
        dirs.append(user)

        # system
        dirs.append("/etc/{}".format(self.name))

        # script dir
        # TODO fucks up tests
        #script_dir = os.path.realpath(sys.path[0])
        #script_dir = os.path.dirname(os.path.realpath(__file__))
        #dirs.append(script_dir)

        return dirs

    def init_dirs_windows(self):
        """initialize config directories for windows
        """
        dirs = []

        # user
        appdata = os.path.expandvars("%APPDATA%/{}".format(self.name))
        appdata = os.path.normpath(appdata)
        dirs.append(appdata)

        # script dir
        script_dir = os.path.realpath(sys.path[0])
        dirs.append(script_dir)

        return dirs

    def find_config(self, dirs):
        """search for config file in directories
        """
        self.log.debug("searching config")
        for dir_ in dirs:
            self.dir = dir_
            filepath = os.path.join(dir_, "default.ini")
            if os.path.exists(filepath):
                self.log.debug("done {}".format(filepath))
                return filepath
        return ""

    def parse(self):
        """parse config file

        filepath in config is relative to config dir

        return dict according to scheme
        not specified or invalid options are set to None
        """
        config = {}

        if self.file:
            self.log.debug("parsing config {}".format(self.file))
            self.parser.read(self.file)
        else:
            self.log.debug("no config file found")
            return config

        config_dir = os.path.dirname(self.file)

        for name, option in self.scheme.items():
            type_ = option["type"]

            if type_ == "str":
                config[name] = self.parser.get(self.name, name, fallback=None)

            # TODO move path handling to own class?
            if type_ == "path":
                path = self.parser.get(self.name, name, fallback=option["default"])

                path = os.path.expanduser(path)

                if not os.path.isabs(path):
                    path = os.path.join(config_dir, path)

                config[name] = os.path.normpath(path)

            # TODO move path handling to own class?
            if type_ == "datapath":
                if not self.parser.get(self.name, name, fallback=None):
                    config[name] = None
                    continue

                path = self.parser.get(self.name, name, fallback=option["default"])
                path = os.path.expanduser(path)

                if not os.path.isabs(path):
                    data = os.getenv("XDG_DATA_HOME")
                    if not data:
                        data = os.path.expanduser("~/.local/share")
                    path = os.path.join(data, path)

                config[name] = os.path.normpath(path)

            if type_ == "int":
                config[name] = self.parser.getint(self.name, name, fallback=None)

            if type_ == "float":
                config[name] = self.parser.getfloat(self.name, name, fallback=None)

            if type_ == "bool":
                config[name] = self.parser.getboolean(self.name, name, fallback=None)

        self.log.debug("done {}".format(config))
        return config


