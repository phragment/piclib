
import os

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Pango

import piclib.events

# TODO add description search field
# TODO add infobox for current file
# - filename
# - open file externally
# - tags and desc
# - width x height ?
# - file size ?

# TODO replace treeview
class SearchPage:

    def __init__(self, view):
        self.view = view
        self.ctrl = view.ctrl

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.tags_ls = Gtk.ListStore(str)
        self.tags_tv = Gtk.TreeView(model=self.tags_ls)
        #self.pack_start(self.tags_tv, True, True, 0)
        #self.tags_tv.set_property("hexpand", True)
        self.tags_tv.set_property("halign", Gtk.Align.FILL)
        self.box.append(self.tags_tv)

        cr = Gtk.CellRendererText()
        # text=0  set cellrenderer attribute "text" to model column 0
        col = Gtk.TreeViewColumn("Tags", cr, text=0)
        col.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        self.tags_tv.append_column(col)

        tags_sel = self.tags_tv.get_selection()
        tags_sel.set_mode(Gtk.SelectionMode.MULTIPLE)

        tags_sel.connect("changed", self.on_tags_sel_changed)

        self.infobox = InfoBox(self.view)
        self.box.append(self.infobox.get_widget())

    def get_widget(self):
        return self.box

    def on_tags_sel_changed(self, selection):
        (model, pathlist) = selection.get_selected_rows()
        tags = []
        for path in pathlist:
            it = model.get_iter(path)
            tag = model.get_value(it, 0)
            if tag:
                tags.append(tag)

        self.ctrl.queue.put(piclib.events.FilterTags(tags))


class InfoBox:

    def __init__(self, view):
        self.view = view
        self.log = view.log

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.label_fn = Gtk.Label()
        self.label_fn.props.wrap = True
        self.label_fn.props.wrap_mode = Pango.WrapMode.WORD_CHAR
        self.label_fn.props.max_width_chars = 20
        self.box.append(self.label_fn)

        self.label_tags = Gtk.Label()
        self.label_tags.props.wrap = True
        self.box.append(self.label_tags)

        self.label_desc = Gtk.Label()
        self.label_desc.props.wrap = True
        self.box.append(self.label_desc)

        self.label_date = Gtk.Label()
        self.box.append(self.label_date)

        self.button_openext = Gtk.Button()
        self.button_openext.set_label("Open")
        self.box.append(self.button_openext)

        self.button_openext.connect("clicked", self.on_openext_clicked)

    def get_widget(self):
        return self.box

    def set_info(self, cimg):
        self.label_fn.set_text(os.path.basename(cimg.fp))
        self.label_tags.set_text(",".join(cimg.tags))
        self.label_desc.set_text(cimg.desc)
        self.label_date.set_text(str(cimg.date))

    def on_openext_clicked(self, button):
        sel = self.view.get_selected()
        for fp in sel:
            self.view.ctrl.queue.put(piclib.events.OpenFile(fp))
            break  # only open first image of selection?











