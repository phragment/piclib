
import os
import time

import piclib.base
import piclib.operator
import piclib.events

class PicLib:

    """
    This is the starting point.

    - parse command line arguments
    - read config file
    - start operator
    - setup signal and exception handling
    - setup logging
    """

    def __init__(self):
        # xdg-user-dir PICTURES
        # ~/.config/user-dirs.dirs
        #picpath = os.getenv("XDG_PICTURES_DIR")
        #if not picpath:
        #    home = os.getenv("HOME")
        #    picpath = os.path.join(home, "Pictures")
        picpath = os.getcwd()

        self.ts_start = time.time()
        self.scheme = {
            "path": {"type": "path", "default": picpath, "positional": "opt"},
            "adaptive": {"type": "bool", "default": False, "desc": "mobile mode"},
            "hidden": {"type": "bool", "default": False, "desc": "show hidden folders and files"}
        }

        app = piclib.base.Application("piclib", self.scheme, self.main, self.stop)
        app.run()

    def main(self, log, settings):
        settings["ts_start"] = self.ts_start
        log.debug("starting")

        self.operator = piclib.operator.Operator(log, settings)

        # open dir
        self.operator.enqueue(piclib.events.ScanDirectory(settings["path"]))

        # switch to dir
        #wd = os.path.abspath(settings["path"])
        #os.chdir(wd)

        self.operator.start()

        log.debug("stopped")

    def stop(self):
        self.operator.enqueue(piclib.events.Quit())




