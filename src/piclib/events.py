
import os
import shutil

import piclib.utils


class Event():

    def process(self):
        raise NotImplementedError("only an interface")


class Quit(Event):

    def process(self, ctrl):
        ctrl.log.debug("quit")
        ctrl.model.stop_scan()
        ctrl.view.stop()
        ctrl.stop()


class ShowImg(Event):

    def __init__(self, fp_img):
        self.fp_img = fp_img

    def process(self, ctrl):
        ctrl.model.open_img(self.fp_img)


class HideImg(Event):

    def process(self, ctrl):
        ctrl.view.hide_img()


class NextImg(Event):

    def __init__(self, cur):
        self.cur = cur

    def process(self, ctrl):
        ctrl.model.open_img_next(self.cur)


class PrevImg(Event):

    def __init__(self, cur):
        self.cur = cur

    def process(self, ctrl):
        ctrl.model.open_img_prev(self.cur)


class FilterTags(Event):

    def __init__(self, tags):
        self.tags = tags

    def process(self, ctrl):
        ctrl.view.filter_tags(self.tags)


# overwrite
class ImgSetTags(Event):

    def __init__(self, fps, tags):
        self.fps = fps
        self.tags = tags

    def process(self, ctrl):
            for fp in self.fps:
                ctrl.model.cache.set_meta(fp, tags=self.tags, set_tags=True)


class ImgAddTags(Event):

    def __init__(self, fps, tags):
        self.fps = fps
        self.tags = tags

    def process(self, ctrl):
            for fp in self.fps:
                ctrl.model.cache.set_meta(fp, tags=self.tags, set_tags=True, overwrite=True)


class ImgSetDesc(Event):

    def __init__(self, fps, desc):
        self.fps = fps
        self.desc = desc

    def process(self, ctrl):
        for fp in self.fps:
                ctrl.model.cache.set_meta(fp, desc=self.desc, set_desc=True)


class OpenFile(Event):

    def __init__(self, fp):
        self.fp = fp

    def process(self, ctrl):
        # TODO spawn in background
        cmd = ["xdg-open", self.fp]
        piclib.utils.run(cmd)


class ScanDirectory(Event):

    def __init__(self, dp):
        self.dp = dp

    def process(self, ctrl):
        ctrl.model.scan_dir(self.dp)


class SwitchDir(Event):

    def __init__(self, dp):
        self.dp = dp

    def process(self, ctrl):
        ctrl.model.stop_scan()

        ctrl.model.view.clear_dir()
        ctrl.model.view.clear_flowbox()
        ctrl.model.clear_tags()

        ctrl.model.scan_dir(self.dp)


class Move(Event):

    def __init__(self, sources, dest):
        self.sources = sources
        self.dest = dest

    def process(self, operator):
        operator.log.debug("move %s to %s", self.sources, self.dest)
        #return

        for fp in self.sources:
            fn = os.path.basename(fp)
            dst_fp = os.path.join(self.dest, fn)
            #operator.log.debug("%s to %s", fp, dst_fp)
            shutil.move(fp, dst_fp)



