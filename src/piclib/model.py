from datetime import datetime
import os
import threading
import time
import zipfile

import gi

gi.require_version('Gtk', '4.0')
from gi.repository import GLib, Gtk, Gio

gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf

gi.require_version('GExiv2', '0.10')
from gi.repository import GExiv2

import piclib.events


class Model:

    def __init__(self, log, settings, view):
        self.log = log.getChild(type(self).__name__)
        self.settings = settings

        self.view = view

        self.stop_loader = False
        self.loader_thread = None

        self.cwd = settings["path"]

        # track currently shown images or all?
        self.files = []
        #self.images = {}
        self.cache = Cache(self)

        self.tags = []

        self.monitor = Monitor(self)

        self.cur_dir = ""

    def scan_archive(self, fp):
        self.log.debug("scanning: %s", fp)

        self.view.update_title(os.path.basename(fp))

        self.view.clear_flowbox()
        self.view.clear_dir()
        self.clear_tags()
        self.view.app.mainwin.imgview.sort_mode = "alpha"
        self.add_dir(os.path.join(os.path.dirname(fp)))

        zf = zipfile.ZipFile(fp)
        infos = zf.infolist()

        files = []
        for info in infos:
            if not info.is_dir():
                files.append(info.filename)

        self.files = []
        cimgs = []
        for fn in sorted(files):
            uri = fp + "?" + fn
            self.files.append(uri)
            cimgs.append( self.cache.get(uri) )

        self.loader_thread = threading.Thread(
            target=self.load,
            name="Loader",
            kwargs={"cimgs": cimgs}
        )
        self.stop_loader = False
        self.loader_thread.start()


    def scan_dir(self, path):
        if not os.path.isabs(path):
            path = os.path.join(self.cwd, path)

        self.cur_dir = path

        if path.endswith(".zip") or path.endswith(".cbz"):
            self.scan_archive(path)
            return

        self.log.debug("scanning: %s", path)

        self.monitor.remove(self.cwd)

        path = os.path.normpath(path)
        self.cwd = path

        self.monitor.add(path)
        self.view.update_title(path)

        cimgs = self.get_filelist(path)

        self.loader_thread = threading.Thread(
            target=self.load,
            name="Loader",
            kwargs={"cimgs": cimgs}
        )
        self.stop_loader = False
        self.loader_thread.start()

    def stop_scan(self):
        self.stop_loader = True
        if self.loader_thread:
            self.loader_thread.join()
            self.log.debug("stopped loader")

    def get_filelist(self, dp):
        self.add_dir(os.path.join(dp, ".."))

        # sort by modified time to load newest first
        files = []
        for de in os.scandir(dp):
            fp = os.path.join(dp, de.name)

            if de.is_dir():
                # TODO support androids .nomedia ?
                if de.name.startswith(".") and not self.settings["hidden"]:
                    continue

                # sort folders alphabetically?
                self.add_dir(fp)
                continue

            stats = de.stat()
            files.append( (stats.st_mtime, fp) )

        files = sorted(files, reverse=True)

        self.files = [f for mtime, f in files]

        cimgs = []
        for mtime, f in files:
            cimgs.append( self.cache.get(f) )

        return cimgs

    def load(self, cimgs):
        for cimg in cimgs:
            if self.stop_loader:
                break

            # slow part
            cimg.load()
            if not cimg.thumb:
                self.cache.remove(cimg.fp)  # TODO test
                continue

            self.view.add_img(cimg)
            #print(cimg.tags)
            self.add_tags(cimg.tags)

        self.log.debug("loaded %s images", len(cimgs))

    def add_tags(self, tags):
        for tag in tags:
            if not tag in self.tags:
                self.tags.append(tag)
                self.view.update_tags(self.tags)

    def clear_tags(self):
        self.tags = []
        self.view.update_tags(self.tags)

    def add_img(self, fp):
        cimg = self.cache.get(fp)
        self.load( [cimg] )

    def add_dir(self, dp):
        self.view.add_dir(dp)

    def add(self, fp):
        if os.path.isdir(fp):
            self.add_dir(fp)
        else:
            self.add_img(fp)

    def remove(self, fp):
        self.view.remove_dir(fp)
        self.view.remove_img(fp)
        self.cache.remove(fp)

    def open_img(self, imgpath):

        # FIXME
        #if imgpath.endswith(".zip") or imgpath.endswith(".cbz"):
        #    self.log.debug("switch to archive %s", imgpath)
        #    self.scan_archive(imgpath)
        #    return

        self.log.debug("showing img %s", imgpath)

        cimg = self.cache.get(imgpath)
        pb = cimg.get_full()

        # TODO pass cimg (for metadata)
        self.view.view_img(imgpath, pb)

    def open_img_next(self, fp):
        i = self.files.index(fp)
        i += 1
        if i > len(self.files) - 1:
            if self.cur_dir.endswith(".zip") or self.cur_dir.endswith(".cbz"):
                #print("switch to next archive", self.cur_dir)
                self.open_archive_next(self.cur_dir)
            return
            #i = 0
        fp = self.files[i]
        self.open_img(fp)

    def open_img_prev(self, fp):
        i = self.files.index(fp)
        i -= 1
        if i < 0:
            if self.cur_dir.endswith(".zip") or self.cur_dir.endswith(".cbz"):
                #print("switch to prev archive", self.cur_dir)
                self.open_archive_prev(self.cur_dir)
            return
            # wrap
            #i = len(self.files) - 1
        fp = self.files[i]
        self.open_img(fp)

    def open_archive_prev(self, fp):
        print(fp)
        fp = fp.split("?")[0]
        fn = os.path.basename(fp)
        dp = os.path.dirname(fp)
        print(fn)

        files = os.listdir(dp)
        files = sorted(files)

        i = files.index(fn)
        print(i)
        i -= 1
        if i < 0:
            return

        prev = os.path.join(dp, files[i])
        print(prev)
        self.view.ctrl.queue.put(piclib.events.SwitchDir(prev))

        zf = zipfile.ZipFile(prev)
        infos = zf.infolist()
        files = []
        for info in infos:
            if not info.is_dir():
                if info.filename.endswith(".jpg"):  # FIXME
                    files.append(info.filename)
        fp_content = sorted(files)[-1]
        print(fp_content)

        #time.sleep(10)  # FIXME
        self.view.ctrl.queue.put(piclib.events.ShowImg(prev + "?" + fp_content))

    def open_archive_next(self, fp):
        print(fp)
        fp = fp.split("?")[0]
        fn = os.path.basename(fp)
        dp = os.path.dirname(fp)
        print(fn)

        files = os.listdir(dp)
        files = sorted(files)

        i = files.index(fn)
        print(i)
        i += 1
        if i > len(files) - 1:
            return

        next_ = os.path.join(dp, files[i])
        print(next_)
        self.view.ctrl.queue.put(piclib.events.SwitchDir(next_))

        zf = zipfile.ZipFile(next_)
        infos = zf.infolist()
        files = []
        for info in infos:
            if not info.is_dir():
                if info.filename.endswith(".jpg"):  # FIXME
                    files.append(info.filename)
        fp_content = sorted(files)[0]
        print(fp_content)

        #time.sleep(10)  # FIXME
        self.view.ctrl.queue.put(piclib.events.ShowImg(next_ + "?" + fp_content))

        # TODO use flowbox selection for prev/next?


class Cache:

    def __init__(self, model):
        self.model = model
        self.log = model.log.getChild(type(self).__name__)

        #if self.model.settings["adaptive"]:
        #    self.thumbsize = 100
        #else:
        #    self.thumbsize = 250
        self.thumbsize = 250

        # use abs filepath as ident
        self.images = {}

    def create(self, fp):
        img = CachedImage(fp, self.thumbsize)
        self.images[fp] = img
        return img

    def get(self, fp):
        fp = os.path.normpath(fp)

        img = self.images.get(fp, None)
        if not img:
            img = self.create(fp)
        return img

    def set_meta(self, fp, tags=[], desc="", set_tags=False, set_desc=False, overwrite=False):
        cimg = self.model.cache.get(fp)
        if tags:
            self.model.add_tags(tags)
        cimg.set_meta(tags, desc, set_tags, set_desc, overwrite, self.log)

    def remove(self, fp):
        img = self.images.pop(fp, None)
        if img:
            del img


class CachedImage:

    def __init__(self, fp, thumbsize, mtime=0):
        if fp.split("?")[0].endswith(".zip") or fp.split("?")[0].endswith(".cbz"):
            self.fp = fp.split("?")[0]
            try:
                self.fp_content = fp.split("?")[1]
            except IndexError:

                zf = zipfile.ZipFile(self.fp)

                infos = zf.infolist()

                files = []
                for info in infos:
                    if not info.is_dir():
                        files.append(info.filename)

                fp = sorted(files)[0]

                self.fp_content = fp
                self.mtime = -1  # TODO set sort mode alphabetical for archives?
        else:
            self.fp = fp
            self.fp_content = fp
        self.mtime = mtime
        self.thumbsize = thumbsize
        self.thumb = None
        self.date = None
        self.tags = []
        self.desc = ""

    def get_id(self):
        return "TODO"

    def load(self):
        if not self.mtime:
            self.load_mtime()

        if not self.thumb:
            self.load_thumb()

        if not self.date:
            self.load_meta()

    def load_mtime(self):
        if self.mtime:
            return
        stats = os.stat(self.fp)
        self.mtime = stats.st_mtime

    def load_thumb(self):

        # handle archive
        if self.fp.endswith(".zip") or self.fp.endswith(".cbz"):
            zf = zipfile.ZipFile(self.fp)

            if self.fp_content == self.fp:
                infos = zf.infolist()

                files = []
                for info in infos:
                    if not info.is_dir():
                        files.append(info.filename)

                fp = sorted(files)[0]
            else:
                fp = self.fp_content

            data = zf.read(fp)
            zf.close()

            data_g = GLib.Bytes.new(data)
            stream = Gio.MemoryInputStream.new_from_bytes(data_g)
            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream)
            except GLib.GError:
                print("could not load", self.fp, self.fp_content)
                return

            self.thumb = pixbuf
            return


        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(self.fp)
        except GLib.GError:
            return

        pixbuf = pixbuf.apply_embedded_orientation()

        # keep aspectratio
        w = pixbuf.get_width()
        h = pixbuf.get_height()
        if w > h:
            dw = self.thumbsize
            dh = int(h / w * self.thumbsize)
        else:
            dw = int(w / h * self.thumbsize)
            dh = self.thumbsize

        pixbuf = pixbuf.scale_simple(
            dw, dh,
            #GdkPixbuf.InterpType.NEAREST
            #GdkPixbuf.InterpType.TILES
            GdkPixbuf.InterpType.BILINEAR  # default
            #GdkPixbuf.InterpType.HYPER
        )

        self.thumb = pixbuf

    def get_full(self):
        if not self.fp == self.fp_content:
            zf = zipfile.ZipFile(self.fp)
            data = zf.read(self.fp_content)
            zf.close()

            data_g = GLib.Bytes.new(data)
            stream = Gio.MemoryInputStream.new_from_bytes(data_g)

            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream)
            except GLib.GError:
                return None

            return pixbuf

        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(self.fp)
        except GLib.GError:
            print("  WARN could not open", self.fp)
            return None
        pixbuf = pixbuf.apply_embedded_orientation()
        return pixbuf

    def load_meta(self):
        meta = GExiv2.Metadata.new()
        try:
            meta.open_path(self.fp)
        except GLib.GError:
            return

        if not meta.get_supports_xmp():
            try:
                meta.open_path(self.fp + ".xmp")
            except GLib.GError:
                return

        datestring = meta.get_tag_string("Exif.Photo.DateTimeOriginal")
        if datestring:
            try:
                dt = datetime.strptime(datestring, "%Y:%m:%d %H:%M:%S")
            except ValueError:
                dt = None
        else:
            dt = None

        tags = meta.get_tag_multiple("Xmp.dc.subject")
        desc = meta.get_tag_string("Xmp.dc.description")
        if desc and desc.startswith("lang="):
            desc = " ".join(desc.split(" ")[1:])
        else:
            desc = ""

        self.date = dt
        self.desc = desc
        self.tags = tags

    # FIXME how to signal valid/empty/add
    def set_meta(self, tags=[], desc="", set_tags=False, set_desc=False, overwrite=False, log=None):

        if log:
            log.debug("set metadata %s %s %s", self.fp, tags, desc)

        # preserve modified time
        stats = os.stat(self.fp)

        meta = GExiv2.Metadata.new()
        # TODO GLib.Error
        meta.open_path(self.fp)
        if not meta.get_supports_xmp():
            ext = True
        else:
            ext = False

        if set_tags:
            if not overwrite:
                tags_old = meta.get_tag_multiple("Xmp.dc.subject")
            else:
                tags_old = []
            meta.clear_tag("Xmp.dc.subject")
            tags += tags_old
            tags = list(set(tags))  # uniq
            meta.set_tag_multiple("Xmp.dc.subject", tags)

            self.tags = tags  # update

        if set_desc:
            meta.clear_tag("Xmp.dc.description")
            meta.set_tag_string("Xmp.dc.description", desc)

            self.desc = desc  # update

        # TODO keep modification date
        if not ext:
            meta.save_file()
        else:
            meta.save_external(self.fp + ".xmp")

        # TODO write to tmp file, then move?

        # preserve modified time
        os.utime(self.fp, (stats.st_atime, stats.st_mtime))

        if log:
            log.debug("set metadata done")


class Monitor:

    def __init__(self, model):
        self.model = model
        self.log = model.log.getChild(type(self).__name__)

        self.monitors = {}

    def add(self, dp):
        self.log.debug("adding file monitor %s", dp)
        gf = Gio.File.new_for_path(dp)
        monitor = gf.monitor_directory(Gio.FileMonitorFlags.WATCH_MOVES)
        monitor.connect("changed", self.on_changed)
        self.monitors[dp] = monitor

    def remove(self, dp):
        self.log.debug("removing file monitor %s", dp)
        fm = self.monitors.get(dp, None)
        if not fm:
            return
        fm.cancel()
        self.monitors[dp] = None

    def on_changed(self, file_monitor, f, of, event_type):
        fp = f.get_path()
        if of:
            fp_new = of.get_path()
        self.log.debug("file event %s %s", event_type, fp)

        if event_type == Gio.FileMonitorEvent.CREATED:
            self.log.debug("created, add")
            self.model.add(fp)

        if event_type == Gio.FileMonitorEvent.MOVED_IN:
            self.log.debug("moved in, add")
            self.model.add(fp)

        if event_type == Gio.FileMonitorEvent.DELETED:
            self.log.debug("deleted, remove")
            self.model.remove(fp)

        if event_type == Gio.FileMonitorEvent.MOVED_OUT:
            self.log.debug("moved out, remove")
            self.model.remove(fp)

        if event_type == Gio.FileMonitorEvent.RENAMED:
            self.log.debug("renamed, remove old add new")
            self.model.remove(fp)
            self.model.add(fp_new)












