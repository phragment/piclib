import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

import piclib.pagesearch
import piclib.pageedit


class SidePane:

    def __init__(self, view):
        self.view = view
        self.ctrl = view.ctrl

        self.box = Gtk.Box()

        self.notebook = Gtk.Notebook()
        self.notebook.set_property("halign", Gtk.Align.FILL)
        self.box.append(self.notebook)

        self.notebook.set_size_request(300 * self.box.get_scale_factor(), -1)

        self.page_search = piclib.pagesearch.SearchPage(self.view)
        self.notebook.append_page(self.page_search.get_widget(), Gtk.Label.new("Search"))

        self.page_edit = piclib.pageedit.EditPage(self.ctrl)
        self.notebook.append_page(self.page_edit.get_widget(), Gtk.Label.new("Editor"))

    def get_widget(self):
        return self.box

    def hide(self):
        self.box.set_visible(False)

    def show(self):
        self.box.set_visible(True)


