
DESTDIR=/
BINDIR=$(DESTDIR)/usr/bin
SYSCONFDIR=$(DESTDIR)/etc
OPTDIR=$(DESTDIR)/opt
DATADIR=$(DESTDIR)/usr/share

.PHONY: all run clean edit lint lint_extra

run:
	./src/piclib.py -v tests/

run_phone:
	./src/piclib.py -v -a tests/

clean:
	-rm -r src/piclib/__pycache__

edit:
	gedit -s src/piclib.py src/piclib/__init__.py src/piclib/operator.py src/piclib/events.py src/piclib/model.py src/piclib/view.py src/piclib/imgview.py src/piclib/pageedit.py src/piclib/pagesearch.py src/piclib/sidepane.py src/piclib/utils.py 2> /dev/null &

install:
	mkdir -p $(BINDIR) $(SYSCONFDIR)/piclib $(OPTDIR)/piclib $(DATADIR)/applications
	cp -r src/* $(OPTDIR)/piclib/
	cp -r config/* $(SYSCONFDIR)/piclib/
	install -Dm755 piclib.sh $(BINDIR)/piclib
	cp piclib.desktop $(DATADIR)/applications/
	cp piclib-adaptive.desktop $(DATADIR)/applications/

uninstall:
	-rm $(SYSCONFDIR)/piclib
	-rm $(OPTDIR)/piclib
	-rm $(BINDIR)/piclib
	-rm $(DATADIR)/applications/piclib.desktop


